package car_dealership;

public class Employee {
	
	public void handleCustomer(Customer cust, boolean finance, Vehicle vehicle) {
		if (finance == true) {
			double loanAmount = vehicle.getPrice() - cust.getCashOnHand();
			runCreditHistory(cust, loanAmount);
		} else if (vehicle.getPrice() <= cust.getCashOnHand()) {
			processTransaction(cust, vehicle);
		} else {
			System.out.println("Customer " + cust.getName() +
					" will need more money to purchase the following vehicle: " +
					vehicle.toString());
		}
	}
	
	public void runCreditHistory(Customer cust, double loanAmount) {
		System.out.println("Run credit check on customer: " + cust.getName());
		System.out.println("The loan amount has been approved, for " + cust.getName());
	}
	
	public void processTransaction(Customer cust, Vehicle vehicle) {
		System.out.println("Customer " + cust.getName() +
				" has purchased the vehicle " + vehicle.toString() + " with cash.");
	}
	
}

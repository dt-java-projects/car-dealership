package car_dealership;

public class Dealership {

	public static void main(String[] args) {
		
		Customer cust1 = new Customer("Tom", "123 Something St.", 11000);
		
		Vehicle vehicle = new Vehicle("BMW", "M3", 13000);
		
		Employee employee = new Employee();
		
		cust1.purchaseCar(vehicle, employee, false);
		
	}

}
